//
//  ViewController.swift
//  RainierRivera-Itunes
//
//  Created by Rainier Rivera on 21/09/2019.
//  Copyright © 2019 Rainier Rivera. All rights reserved.
//

import UIKit

class TrackListViewController: UIViewController {

    var viewModel: TrackListViewModel!
    
    private struct Constants {
        static let identifier = "TrackTableViewCell"
    }
    
    @IBOutlet weak private var tableView: UITableView!
    
    
    // MARK: Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViewModel()
        configureNavigationBar()
        configureTableView()
        
        viewModel.getTrackList { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    
    // MARK: Actions
    @IBAction private func favouriteAction(_ sender: AnyObject) {
        guard let favouriteViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FavouritesViewController") as? FavouritesViewController else {
            return
        }
        
        let favouriteViewModel = FavouriteViewModel()
        favouriteViewController.viewModel = favouriteViewModel
        
        navigationController?.pushViewController(favouriteViewController, animated: true)
    }
    
    // MARK: Private method
    
    private func setupViewModel() {
        self.viewModel = TrackListViewModel()
    }
    private func configureNavigationBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = viewModel.navigationTitle
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = UITableView.automaticDimension
        let trackCell = UINib(nibName: Constants.identifier, bundle: nil)
        tableView.register(trackCell, forCellReuseIdentifier: Constants.identifier)
    }
    
}

extension TrackListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.identifier, for: indexPath) as? TrackTableViewCell else {
            fatalError()
        }
        
        let track = viewModel.trackIn(row: indexPath.row)
        
        let viewModel = TrackTableViewModel(track: track)
        cell.setupViewModel(viewModel)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let detailViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController else {
            return
        }
        let track = viewModel.trackIn(row: indexPath.row)
        let viewModel = DetailViewModel(track: track)
        
        detailViewController.viewModel = viewModel
        splitViewController?.showDetailViewController(detailViewController, sender: nil)
        //navigationController?.pushViewController(detailViewController, animated: true)
    }
    
}

extension TrackListViewController: TrackTableViewCellDelegate {
    func trackTableViewCellDelegate(cell: TrackTableViewCell, didFavourite: Bool, track: Track) {
        viewModel.favouriteTrack(didFavourite, track: track)
    }

}
