//
//  ItunesAPI.swift
//  RainierRivera-Itunes
//
//  Created by Rainier Rivera on 21/09/2019.
//  Copyright © 2019 Rainier Rivera. All rights reserved.
//

import Foundation

protocol ItunesAPI {
    func getTrackList(completion: @escaping (APIResponse<TrackResponse>) -> Void)
    // and so on
}
