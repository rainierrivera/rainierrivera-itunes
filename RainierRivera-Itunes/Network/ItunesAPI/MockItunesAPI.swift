//
//  MockItunesAPI.swift
//  RainierRivera-Itunes
//
//  Created by Rainier Rivera on 22/09/2019.
//  Copyright © 2019 Rainier Rivera. All rights reserved.
//

import Foundation

class MockItunesAPI: ItunesAPI {
    func getTrackList(completion: @escaping (APIResponse<TrackResponse>) -> Void) {
        completion(.success(TrackFactory.makeTrack()))
    }
}
