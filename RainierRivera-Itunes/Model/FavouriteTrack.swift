//
//  FavouriteTrack.swift
//  RainierRivera-Itunes
//
//  Created by Rainier Rivera on 22/09/2019.
//  Copyright © 2019 Rainier Rivera. All rights reserved.
//

import Foundation

class FavouriteTrack: Codable {
    var tracks: [Track]
    
    init(tracks: [Track]) {
        self.tracks = tracks
    }
}
