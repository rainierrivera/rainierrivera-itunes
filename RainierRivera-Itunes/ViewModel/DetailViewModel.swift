//
//  DetailViewModel.swift
//  RainierRivera-Itunes
//
//  Created by Rainier Rivera on 22/09/2019.
//  Copyright © 2019 Rainier Rivera. All rights reserved.
//

import Foundation

class DetailViewModel: CustomStringConvertible {
    
    private let track: Track
    init(track: Track) {
        self.track = track
    }
    
    var description: String {
        return track.description ?? "No description" // This should be in localized
    }
    
    var navigationTitle: String {
        return track.trackName ?? .empty
    }
}
